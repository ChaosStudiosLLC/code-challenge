﻿using API.Factories.FactoryInterfaces;
using API.Models;
using API.TreeFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Factories.TreeFactories
{
    public class TreeNodeFactory
    {
        public FactoryTreeNode TreeNode;

        public void CreateTreeNode(TreeNodeModel factory, TreeNodeFactory parent)
        {
            TreeNode = new FactoryTreeNode(factory, parent);
        }

        public FactoryTreeNode GetTreeNode()
        {
            return TreeNode;
        }


    }
}