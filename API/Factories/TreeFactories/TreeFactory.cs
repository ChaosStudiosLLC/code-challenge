﻿using API.Factories.FactoryInterfaces;
using API.TreeFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Factories.TreeFactories
{
    public class TreeFactory
    {
        Tree Tree;

        public Tree CreateTree()
        {
            Tree = new Tree();
            return Tree;
        }
    }
}