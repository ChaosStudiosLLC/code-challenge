﻿using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    [Validator(typeof(TreeNodeModelValidator))]
    public class TreeNodeModel
    {
        public int? index;
        public string Name;
        public int ChildCount;
        public int Upperbound;
        public int Lowerbound;
    }
}