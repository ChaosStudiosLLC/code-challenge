﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class TreeNodeModelValidator : AbstractValidator<TreeNodeModel>
    {
        /// <summary>  
        /// Validator rules for Product  
        /// </summary>  
        public TreeNodeModelValidator()
        {
            RuleFor(x => x.Name).NotEmpty()
                .WithMessage("The Product Name cannot be blank.");

            RuleFor(x => x.ChildCount).GreaterThan(0).WithMessage("The Child Count must be at greather than 0.")
                .InclusiveBetween(1, 15).WithMessage("The Child Count must be between 1 and 15.");

            RuleFor(x => x.Upperbound).GreaterThan(0).WithMessage("The Upperbound must be at greather than 0.")
                .When(x => x.Upperbound <= x.Lowerbound).WithMessage("The Upperbound must be greather than the Lowerbound.")
                .When(x => x.Upperbound - x.Lowerbound < x.ChildCount).WithMessage("There must be a diffrence of at least Child Count between Upperbound and Lowerbound.");

            RuleFor(x => x.Lowerbound).GreaterThan(0).WithMessage("The Lowerbound must be at greather than 0.")
                .When(x => x.Upperbound <= x.Lowerbound).WithMessage("The Lowerbound must be lesser than the Upperbound.")
                .When(x => x.Upperbound - x.Lowerbound < x.ChildCount).WithMessage("There must be a diffrence of at least Child Count between Upperbound and Lowerbound.");

        }
    }
}