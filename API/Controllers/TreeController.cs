﻿using API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using API.TreeFunctions;
using API.Models;
using API.Factories;
using API.Factories.FactoryInterfaces;
using API.Factories.TreeFactories;
using Newtonsoft.Json;
using System.Web.Http.Description;
using System.IO;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using System.Text;
using System.Threading;
using System.Collections.Concurrent;
using Microsoft.Web.WebSockets;
using System.Web;

namespace API.Controllers
{
    public class TreeController : ApiController
    {

        [Route("api/updateTree")]
        public IHttpActionResult UpdateTree(TreeNodeModel treeNode)
        {
            Tree tree = (Tree)State.GetValue("treeData");

            if (tree == null)
            {
                TreeFactory treeFactory = new TreeFactory();
                // Creates Tree
                tree = treeFactory.CreateTree();
            }

            FactoryTreeNode updateNode = null;

            if (treeNode.index != null)
            {
                updateNode = tree.Find((int)treeNode.index);
            }

            if (updateNode == null)
            {
                // Adds Factory
                tree.AddFacotry(treeNode);
            }
            else
            {
                updateNode.Update(treeNode);
            }

            State.Update("treeData", tree);

            return Ok("Updated Tree");
        }

        [Route("api/removeNode")]
        public IHttpActionResult RemoveFactory(TreeNodeModel treeNode)
        {
            Tree tree = (Tree)State.GetValue("treeData");

            tree.RemoveNode(treeNode);

            State.Update("treeData", tree);

            return Ok("Removed factory from tree");
        }

        [Route("api/clearTree")]
        [HttpGet]
        public IHttpActionResult ClearTree()
        {
            Tree tree = (Tree)State.GetValue("treeData");

            tree.Clear();

            State.Update("treeData", tree);

            return Ok("Cleared Tree");
        }

    }
}
