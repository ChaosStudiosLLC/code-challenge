﻿using API.Services;
using API.TreeFunctions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using System.Web.Http;
using System.Web.WebSockets;

namespace API.Controllers
{
    public class WsTimeController : ApiController
    {
        private static ConcurrentBag<StreamWriter> clients;
        private static Random random;
        private static Timer timer;

        static WsTimeController()
        {
            clients = new ConcurrentBag<StreamWriter>();

            timer = new Timer();
            timer.Interval = 1000;
            timer.AutoReset = true;
            timer.Elapsed += timer_Elapsed;
            timer.Start();

            random = new Random();
        }

        [Route("api/connect")]
        [HttpGet]
        public IHttpActionResult Get(HttpRequestMessage request)
        {
            var response = request.CreateResponse();
            response.Content = new PushStreamContent((a, b, c) =>
            { OnStreamAvailable(a, b, c); }, "text/event-stream");
            return ResponseMessage(response);
        }

        private async static void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Tree tree = (Tree)State.GetValue("treeData");

            foreach (var client in clients)
            {
                try
                {
                    JsonSerializerSettings settings = new JsonSerializerSettings();
                    settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    var data = string.Format("data: {0}\n\n", JsonConvert.SerializeObject(tree, settings));
                    await client.WriteAsync(data);
                    await client.FlushAsync();
                }
                catch (Exception)
                {
                    StreamWriter ignore;
                    clients.TryTake(out ignore);
                }
            }

        }

        private void OnStreamAvailable(Stream stream, HttpContent content,
        TransportContext context)
        {
            var client = new StreamWriter(stream);
            clients.Add(client);
        }
    }
}