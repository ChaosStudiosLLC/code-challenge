﻿using API.Factories.FactoryInterfaces;
using API.Factories.TreeFactories;
using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace API.TreeFunctions
{
    public class FactoryTreeNode
    {
        public string Name { get; set; }
        public TreeNodeFactory Parent { get; set; }
        public int ChildCount { get; set; }
        public int Upperbound { get; set; }
        public int Lowerbound { get; set; }
        public IList<NumberTreeNode> Children { get; set; }

        public FactoryTreeNode(TreeNodeModel treeNode, TreeNodeFactory parent)
        {
            Name = treeNode.Name;
            Parent = parent;
            ChildCount = treeNode.ChildCount;
            Upperbound = treeNode.Upperbound;
            Lowerbound = treeNode.Lowerbound;
            GenerateChildren();
        }

        public void Update(TreeNodeModel treeNode)
        {
            Name = treeNode.Name;
            if (ChildCount != treeNode.ChildCount || Upperbound != treeNode.Upperbound || Lowerbound != treeNode.Lowerbound)
            {
                ChildCount = treeNode.ChildCount;
                Upperbound = treeNode.Upperbound;
                Lowerbound = treeNode.Lowerbound;
                GenerateChildren();
            }
        }

        public bool RemoveAllChildren()
        {
            Children.Clear();

            return true;
        }

        public void GenerateChildren()
        {
            if (Children == null)
            {
                Children = new List<NumberTreeNode>();
            }

            if (Children.Count() > 0)
            {
                RemoveAllChildren();
            }
            Random rnd = new Random();

            while (Children.Count < ChildCount)
            {
                int number = rnd.Next(Lowerbound, Upperbound );
                if (Children.AsQueryable().Where(x => x.Value == number).Count() == 0) Children.Add(new NumberTreeNode(number, this));
            }
        }
    }
}