﻿using API.Factories.FactoryInterfaces;
using API.Factories.TreeFactories;
using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace API.TreeFunctions
{
    public class Tree
    {
        public TreeNodeFactory Root { get; set; }
        public IList<TreeNodeFactory> Nodes { get; set; }

        public Tree()
        {
            Nodes = new List<TreeNodeFactory>();
        }

        public void AddFacotry(TreeNodeModel treeNode)
        {
            if (Nodes.AsQueryable().Where(x => x.GetTreeNode().Name == treeNode.Name).Count() == 0)
            {
                TreeNodeFactory factory = new TreeNodeFactory();
                if (Root == null)
                {
                    factory.CreateTreeNode(treeNode, null);
                    Root = factory;
                }
                else
                {
                    factory.CreateTreeNode(treeNode, Root);
                }

                factory.GetTreeNode().GenerateChildren();
                Nodes.Add(factory);
            }
        }

        public void Clear()
        {
            Nodes.Clear();

            Root = null;
        }

        public bool RemoveNode(TreeNodeModel treeNode)
        {
            if (Nodes.Count >= treeNode.index)
            {
                Nodes.Remove(Nodes.ElementAt((int)treeNode.index));

                if (Nodes.Count > 0)
                {
                    if (Root.TreeNode.Name == treeNode.Name)
                    {
                        TreeNodeFactory rootfactory = Nodes.FirstOrDefault();

                        rootfactory.TreeNode.Parent = null;

                        Root = rootfactory;

                        foreach (TreeNodeFactory factory in Nodes.Skip(1))
                        {
                            factory.TreeNode.Parent = Root;
                        }
                    }
                }
                else
                {
                    Root = null;
                }

                return true;
            }

            return false;

        }

        public FactoryTreeNode Find(int index)
        {
            if(Nodes.Count >= index)
            {
                return Nodes.ElementAt(index).GetTreeNode();
            }
            
            return null;
        }

        public FactoryTreeNode Find(string name)
        {
            foreach (TreeNodeFactory node in Nodes)
            {
                if (node.GetTreeNode().Name.Equals(name))
                {
                    return node.GetTreeNode();
                }
            }
            return null;
        }

    }
}