﻿using API.Factories.FactoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace API.TreeFunctions
{
    public class NumberTreeNode
    {
        public int Value { get; set; }
        public FactoryTreeNode Parent { get; set; }

        public NumberTreeNode(int value, FactoryTreeNode parent)
        {
            Value = value;
            Parent = parent;
        }

        public override string ToString()
        {
            StringBuilder nodeString = new StringBuilder();
            nodeString.Append("[Node Value:" + Value + " Parent: ");
            if (Parent != null)
            {
                nodeString.Append(Parent.Name);
            }
            else
            {
                nodeString.Append("null");
            }

            nodeString.Append("]");

            return nodeString.ToString();
        }
    }
}