﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Caching;

namespace API.Services
{
    public class State
    {
        public static object GetValue(string key)
        {
            Cache cache = new Cache();
            return cache.Get(key);

        }

        public static bool Add(string key, object value)
        {
            Cache cache = new Cache();
            cache.Insert(key, value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration);
            return true;
        }

        public static void Delete(string key)
        {
            Cache cache = new Cache();
            object cacheObject = cache.Get(key);
            if (cacheObject != null)
            {
                cache.Remove(key);
            }
        }

        public static void Update(string key, object value)
        {
            Delete(key);
            Add(key, value);
        }
    }
}