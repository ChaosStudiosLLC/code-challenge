const Site = {
  url: location.hostname === "localhost"?'https://localhost:44355/api':'https://treecodechallenge.azurewebsites.net/api',
}

export default Site;
