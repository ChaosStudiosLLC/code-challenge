import { Record } from 'immutable';
import FactoryTreeNodeModel from './FactoryTreeNodeModel';

export default Record({
  value: "",
  parent: new FactoryTreeNodeModel(),
});
