import { Record, List } from 'immutable';

export default Record({
  name: "",
  parent: null,
  childCount: null,
  upperbound: null,
  lowerbound: null,
  children: new List(),
});
