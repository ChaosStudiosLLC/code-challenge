import { Record } from 'immutable';
import FactoryTreeNodeModel from './FactoryTreeNodeModel';

export default Record({
  root : new FactoryTreeNodeModel(),
  nodes: [],
});
