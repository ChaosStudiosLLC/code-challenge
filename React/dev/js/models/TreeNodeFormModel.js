import { Record } from 'immutable'

export default Record({
  index: null,
  name: "",
  childCount: 0,
  upperbound: 0,
  lowerbound: 0,
});
