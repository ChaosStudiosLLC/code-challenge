import React from 'react';
import Tree from '../containers/tree';
import { Col } from 'react-bootstrap';
require('../../scss/style.scss');

const App = () => (
    <Col md={12}>
      <Col md={12}>
        <Col md={3} mdOffset={6}>
          <h1>Factory List</h1>
        </Col>
      </Col>
      <Tree />
    </Col>
);

export default App;
