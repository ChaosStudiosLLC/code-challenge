import {Map, List} from 'immutable';

import TreeModel from '../models/TreeModel';
import TreeNodeFormModel from '../models/TreeNodeFormModel';

import * as Actions from '../actions';

const initialState = new Map({
  treeNode: new TreeNodeFormModel(),
  tree: new TreeModel(),
  errors: [],
});

function findTreeNode(nodes,name){
  const factory = nodes.filter((item)=> item.treeNode.name === name);
  return new TreeNodeFormModel({index: nodes.indexOf(factory[0]), name: factory[0].treeNode.name, childCount: factory[0].treeNode.childCount, upperbound: factory[0].treeNode.upperbound, lowerbound: factory[0].treeNode.lowerbound})
}

export default function (state, action) {

    switch (action.type) {
        case Actions.SET_TREE:{
          const value = new TreeModel(action.payload);
          return state.withMutations(state => {
            state.set('tree', value);
          });
        }

        case Actions.SET_ERRORS:{
          return state.withMutations(state => {
            state.set('errors', action.payload);
          });
        }

        case Actions.RESET_ERRORS:{
          return state.withMutations(state => {
            state.set('errors', []);
          });
        }

        case Actions.SET_TREE_NODE:{
          const value = `treeNode.${action.payload.id}`;
          return state.setIn(value.split('.'), action.payload.value);
        }

        case Actions.UPDATE_TREE_NODE:{
          const nodes = state.get('tree').nodes;
          return state.set('treeNode', findTreeNode(nodes, action.payload));
        }

        case Actions.CLEAR_TREE_NODE_FORM:{
          const nodes = state.get('tree').nodes;
          return state.set('treeNode', new TreeNodeFormModel());
        }

        case Actions.CLEAR_TREE:
        case Actions.REMOVE_TREE_NODE:{
          const value = `treeNode.index`;
          return state.setIn(value.split('.'), null);
        }

        case Actions.UPDATE_TREE:{
          return state;
        }

        default:
        return initialState;
    }
}
