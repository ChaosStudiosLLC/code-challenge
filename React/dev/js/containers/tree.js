import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import autobind from 'class-autobind';
import {connect} from 'react-redux';
import {setCount} from '../actions/index';
import axios from 'axios';
import toastr from 'toastr';
import { Row,Col,Button, Label } from 'react-bootstrap';
import TreeNodeFormModel from '../models/TreeNodeFormModel';
import {setTree,setTreeNode, updateTreeNode, updateTree,
 removeTreeNode, clearTreeNode, setErrors, resetErrors,
 clearTreeNodeForm} from '../actions/index';
import TreeSelector from '../selectors/treeSelector';
import TreeNodeSelector from '../selectors/TreeNodeSelector';
import ErrorSelector from '../selectors/ErrorSelector';
import TreeNode from './treeNode';
import {get,post} from '@truefit/http-utils';
import NumericInput from '../formComponents/numericInput';
import TextInput from '../formComponents/textInput';
import ReadOnlyText from '../formComponents/ReadOnlyText';
import Site from '../Global';

class Tree extends Component {

  constructor (props) {
    super(props)
    autobind(this);

    this.state = { connected: "Not Connected", message: "" }
  }

  componentWillMount() {

  }

  componentDidMount() {
    const {setTree} = this.props;
    const source = Site.url;
    this.eventSource = new EventSource(source + '/connect');
    this.eventSource.onopen = () =>{
      this.setState({connected: "Connected"});
    }
    this.eventSource.onmessage = e =>
    setTree(JSON.parse(e.data));

  }

  componentWillUnmount() {
  }

  updates(){

  }

  handleUpdateTreeClick(){
    const {updateTree, setErrors, resetErrors, treeNode, tree} = this.props;
    event.preventDefault();

    const errors = [];

    if(treeNode.name === ""){
      errors.push({id: "name", message:"Name must not be empty!"});
    }
    if(tree.nodes.filter((item)=> item.treeNode.name === treeNode.name).length > 0 && treeNode.index === null){
      errors.push({id: "name", message:"Name is already in tree!"});
    }
    if(treeNode.childCount <= 0 || treeNode.childCount >= 16){
      errors.push({id: "childCount", message:"Child Count must be greater than 0 and less than 16!"});
    }
    if(treeNode.lowerbound <= 0){
      errors.push({id: "lowerbound", message:"Lowerbound must be greater than 0!"});
    }
    if(treeNode.lowerbound >= 2147483647){
      errors.push({id: "lowerbound", message:"Lowerbound can not be larger than 2,147,483,647!"});
    }
    if(treeNode.upperbound <= 0 || treeNode.upperbound >= 2147483647){
      errors.push({id: "upperbound", message:"Upperbound must be greater than 0!"});
    }
    if(treeNode.upperbound >= 2147483647){
      errors.push({id: "upperbound", message:"Upperbound can not be larger than 2,147,483,647!"});
    }
    if(treeNode.childCount > treeNode.upperbound - treeNode.lowerbound){
      errors.push({id: "upperbound", message:"There must be a diffrence of at least Child Count between Upperbound and Lowerbound!"});
    }

    if(errors.length > 0){
        setErrors(errors);
    }
    else{
        updateTree(treeNode)
        .then((result)=> {
          resetErrors();
        });
    }
  }

  handleClearTreeClick(){
    const {clearTreeNode} = this.props;
    event.preventDefault();
    clearTreeNode();
  }

  handleRemoveNodeClick(){
    const {removeTreeNode,treeNode} = this.props;
    event.preventDefault();
    removeTreeNode(treeNode);
  }

  handleUpdateForm(id, value){
    const{setTreeNode} = this.props;
    setTreeNode(id, value);
  }

  handleFactoryClick(event){
    const{updateTreeNode} = this.props;
    event.preventDefault();
    const node = event.currentTarget.dataset.node;
    updateTreeNode(node);
  }

  handleClearTreeFormClick(){
    const{clearTreeNodeForm} = this.props;
    event.preventDefault();
    clearTreeNodeForm();
  }

    render() {
      const{tree, treeNode, errors} = this.props;
        return (
          <Col className="content">
            <Row>
              <Col md={1} className='connection-box'>
                <ReadOnlyText label="Connection:" className={this.state.connected === 'Connected'?'connected':'not-connected'} value={this.state.connected} />
              </Col>
              <Col md={12} className="treeNodes">
                {tree === undefined ? null : tree.nodes.map((node, key) => <TreeNode key={key} node={node} onClick={this.handleFactoryClick}/>)}
              </Col>
            </Row>
            <Row>
              <Col md={4} className="treeNodeForm">
                <Col md={12}>
                  <Col md={2} mdOffset={5}>
                    <Label className='header-label'>Factory Form</Label>
                  </Col>
                  <Col md={12}>
                    <ReadOnlyText label="Selected:" value={treeNode.index === null? 'none': treeNode.name} />
                  </Col>
                </Col>
                <Col md={12}>
                  <Col md={6}>
                    <TextInput id="name" label="Factory Name" placeholder="Name" onChange={this.handleUpdateForm} value={treeNode.name} errors={errors}/>
                    <NumericInput id="childCount" label="Child Count" onChange={this.handleUpdateForm} value={treeNode.childCount} errors={errors} />
                  </Col>
                  <Col md={6}>
                    <NumericInput id="lowerbound" label="Lowerbound" onChange={this.handleUpdateForm} value={treeNode.lowerbound} errors={errors}/>
                    <NumericInput id="upperbound" label="Upperbound" onChange={this.handleUpdateForm} value={treeNode.upperbound} errors={errors}/>
                  </Col>
                  <Col md={12}>
                    <Col md={6} className="margin-left-0">
                      <Col md={6} className="padding-left-0">
                        <Button onClick={this.handleUpdateTreeClick}>Update Tree</Button>
                      </Col>
                      <Col md={6} className="padding-left-0">
                        <Button onClick={this.handleRemoveNodeClick}>Remove Factory</Button>
                      </Col>
                    </Col>
                    <Col md={6} className="margin-right-0">
                      <Col md={6}>
                        <Button onClick={this.handleClearTreeClick}>Clear Tree</Button>
                      </Col>
                      <Col md={6} className="padding-right-0">
                        <Button onClick={this.handleClearTreeFormClick}>Clear Form</Button>
                      </Col>
                    </Col>
                  </Col>
                </Col>
              </Col>
            </Row>
          </Col>
        );
    }

}

function mapStateToProps(state) {
    return {
        tree: TreeSelector(state),
        treeNode: TreeNodeSelector(state),
        errors: ErrorSelector(state),
    };
}


export default connect(mapStateToProps, {setTree, setTreeNode, updateTreeNode,
  updateTree, removeTreeNode, clearTreeNode, setErrors, resetErrors, clearTreeNodeForm})(Tree);
