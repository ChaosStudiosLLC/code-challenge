import React, {Component} from 'react';
import {connect} from 'react-redux';
import autobind from 'class-autobind';
import PropTypes from 'prop-types';
import {Panel, Col} from 'react-bootstrap';
import InlineReadOnlyText from '../formComponents/InlineReadOnlyText';

class TreeNode extends Component {
    constructor (props) {
      super(props)
      autobind(this);
    }

    render() {
      const {key, node, onClick} = this.props;
      return(
        <Col md={2} className="treeNodePanel" key={key} data-node={node.treeNode.name} onClick={onClick}>
          <Panel>
            <Panel.Heading>
              <Panel.Title componentClass="h3">{node.treeNode.name}</Panel.Title>
            </Panel.Heading>
            <Panel.Body>
              <InlineReadOnlyText label="Parent:" value={node.treeNode.parent === null ? '' : node.treeNode.parent.treeNode.name} />
              <InlineReadOnlyText label="Upperbound:" value={node.treeNode.upperbound} />
              <InlineReadOnlyText label="Lowerbound:" value={node.treeNode.lowerbound} />
              <InlineReadOnlyText label="Child Count:" value={node.treeNode.childCount} />
              <InlineReadOnlyText label="Children:" value={node.treeNode.children === null ? '0' : node.treeNode.children.map((child, index) => (index === node.treeNode.children.length-1) ? `${child.value}` : `${child.value},`)} />
            </Panel.Body>
          </Panel>
        </Col>
      );

    }
}

TreeNode.propTypes = {
  node: PropTypes.object,
  key: PropTypes.string,
  onClick: PropTypes.func
}

export default connect(null)(TreeNode);
