import { createSelector } from 'reselect';
import TreeReducerSelector from './TreeReducerSelector';

//const treeReducer = state => state.treeReducer;

export default createSelector(
  TreeReducerSelector,
  state => state.get('tree'),
);

// export const treeNodeSelector = createSelector(
//   TreeSelector,
//   state => state.treeNode,
// );
