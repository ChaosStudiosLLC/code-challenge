import { createSelector } from 'reselect';

const UserReducerSelector = state => state.userReducer;

export const userSelector = createSelector(
  UserReducerSelector,
  user => user,
);
