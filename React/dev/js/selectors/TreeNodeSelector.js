import { createSelector } from 'reselect';
import TreeReducerSelector from './TreeReducerSelector';

export default createSelector(
  TreeReducerSelector,
  state => state.get('treeNode'),
);
