import {get,post} from '@truefit/http-utils';

export const SET_TREE = "SET_TREE";
export const SET_ERRORS = "SET_ERRORS";
export const RESET_ERRORS = "RESET_ERRORS";
export const SET_TREE_NODE = "SET_TREE_NODE";
export const CLEAR_TREE_NODE_FORM = "CLEAR_TREE_NODE_FORM";
export const UPDATE_TREE_NODE = "UPDATE_TREE_NODE";
export const UPDATE_TREE = "UPDATE_TREE";
export const CLEAR_TREE = "CLEAR_TREE";
export const REMOVE_TREE_NODE = "REMOVE_TREE_NODE";

export function setTree (tree) {
    return {
        type: 'SET_TREE',
        payload: tree
    }
};

export function setErrors (errors) {
    return {
        type: 'SET_ERRORS',
        payload: errors
    }
};

export function resetErrors (errors) {
    return {
        type: 'RESET_ERRORS',
        payload: errors
    }
};

export function setTreeNode(id, value) {
    return {
        type: 'SET_TREE_NODE',
        payload: {id, value}
    }
};

export function updateTreeNode(value) {
    return {
        type: 'UPDATE_TREE_NODE',
        payload: value
    }
};

export function clearTreeNodeForm() {
    return {
        type: 'CLEAR_TREE_NODE_FORM'
    }
};

export function updateTree(model) {
    return {
        type: 'UPDATE_TREE',
        payload: post('/updateTree', model)
    }
};


export function removeTreeNode(model) {
    return {
        type: 'REMOVE_TREE_NODE',
        payload: post('/removeNode', model)
    }
};

export function clearTreeNode() {
    return {
        type: 'CLEAR_TREE',
        payload: get('/clearTree')
    }
};
