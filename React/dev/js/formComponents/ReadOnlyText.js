import React, {Component} from 'react';
import {connect} from 'react-redux';
import autobind from 'class-autobind';
import PropTypes from 'prop-types';
import {Col, Label} from 'react-bootstrap';

class ReadOnlyText extends Component {
    constructor (props) {
      super(props)
      autobind(this);
    }


    render() {
      const {label, value, className} = this.props;
      return(
        <Col md={12} >
          <Label>{label}</Label><div className={className}>{value}</div>
        </Col>
      );

    }
}

ReadOnlyText.propTypes = {
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  className: PropTypes.string,
}

export default connect(null)(ReadOnlyText);
