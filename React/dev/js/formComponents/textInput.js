import React, {Component} from 'react';
import {connect} from 'react-redux';
import autobind from 'class-autobind';
import PropTypes from 'prop-types';
import {Col,ControlLabel, FormControl, HelpBlock} from 'react-bootstrap';

class TextInput extends Component {
    constructor (props) {
      super(props)
      autobind(this);
    }


    handleChange(event){
      const{onChange} = this.props;
      let value = event.target.value.replace(/[^a-zA-Z0-9- ]/g, '');
      onChange(event.target.id, value);
    }

    render() {
      const {id, label, value, placeholder, errors} = this.props;
      return(
        <Col md={12} >
          <ControlLabel>{label}</ControlLabel>
          <FormControl
            id={id}
            type="text"
            value={value}
            placeholder={placeholder}
            onChange={this.handleChange}
          />
          <FormControl.Feedback />
          <HelpBlock>No symbol characters allowed</HelpBlock>

          {errors.filter((item) => item.id === id).map((error, key) =><span key={key} className="errorMessage">{error.message}</span>)}
        </Col>
      );

    }
}

TextInput.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  errors: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
}

export default connect(null)(TextInput);
