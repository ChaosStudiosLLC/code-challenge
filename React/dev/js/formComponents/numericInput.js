import React, {Component} from 'react';
import {connect} from 'react-redux';
import autobind from 'class-autobind';
import PropTypes from 'prop-types';
import {Col,ControlLabel, FormControl, HelpBlock} from 'react-bootstrap';

class NumericInput extends Component {
    constructor (props) {
      super(props)
      autobind(this);
    }

    isNumeric(number){
      if(!isNaN(number)){
        return true;
      }
    }

    handleChange(event){
      const{onChange} = this.props;
      let value = event.target.value.replace(/[^\d]/g, '');
      onChange(event.target.id, Number(value));
    }

    render() {
      const {id, label, value, placeholder, errors} = this.props;
      const arrayerror = errors.filter((item) => item.id === id);
      return(
        <Col md={12} >
          <ControlLabel>{label}</ControlLabel>
          <FormControl
            id={id}
            type="text"
            value={value}
            maxLength="10"
            placeholder={placeholder}
            onChange={this.handleChange}
          />
          <FormControl.Feedback />
          <HelpBlock>Only numeric characters allowed</HelpBlock>

          {errors.filter((item) => item.id === id).map((error, key) =><Col md={12}><span key={key} className="errorMessage">{error.message}</span></Col>)}
        </Col>
      );

    }
}

NumericInput.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  errors: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
}

export default connect(null)(NumericInput);
