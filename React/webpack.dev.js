 const merge = require('webpack-merge');
 const common = require('./webpack.common.js');

 module.exports = merge(common, {
   mode: 'development',
   devServer: {
       disableHostCheck: true,
       inline: true,
       contentBase: './src',
       port: 3000,
   },
   devtool: 'inline-source-map',
 });
