const path = require('path');
 var webpack = require('webpack');

 const ASSET_PATH = process.env.ASSET_PATH || '/';

 module.exports = {
    mode:'development',
     entry: './dev/js/index.js',
     module: {
         rules: [
             {
               test: /\.m?js$/,
               exclude: /(node_modules|bower_components)/,
               use: {
                 loader: 'babel-loader',
                 options: {
                   presets: ['@babel/preset-env']
                 }
               }
             },
             {
                 test: /\.scss/,
                 use: ['style-loader', 'css-loader', 'sass-loader']
             },
         ]
     },
     output: {
         path:  path.resolve(__dirname, 'src'),
         filename: 'js/bundle.min.js',
         publicPath:ASSET_PATH
     },
     plugins: [
         new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.DefinePlugin({
      'process.env.ASSET_PATH': JSON.stringify(ASSET_PATH)
    })
     ]
 };
